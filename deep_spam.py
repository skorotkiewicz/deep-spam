"""
CLI for deep spam.

"""

import argparse
import sys
import os
import tensorflow as tf

from email_classification import main
import email_feature_generator
import globals
FLAGS = None


def load_data(data_dir, mode):
    if not os.path.isdir(data_dir):
        print('Path %s must exist, or another path may be specified with --data-dir')
        os.exit(1)
    globals.token_classifier = email_feature_generator.TokenClassifier(data_dir, mode=mode)
    globals.email_feature_generator = email_feature_generator.EmailFeatureGenerator(globals.FEATURE_COUNT)
    globals.email_tokenizer = email_feature_generator.EmailTokenizer()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='./data',
                        help='Directory for storing trained data; defaults to ./data')
    action_group = parser.add_mutually_exclusive_group(required=True)
    action_group.add_argument('--spam', action='store_true', help='Train message(s) as spam')
    action_group.add_argument('--ham', action='store_true', help='Train message(s) as ham')
    action_group.add_argument('--classify', action='store_true', help='Classify a single message')
    parser.add_argument('input', nargs='*', default=sys.stdin, type=argparse.FileType('r'),
                        help='File(s) where message(s) are located, read from stdin')
    FLAGS, unparsed = parser.parse_known_args()

    data_dir = FLAGS.data_dir
    fs = FLAGS.input
    if FLAGS.classify:
        load_data(data_dir, 'r')
        # classify_messages(fs)
    else:
        load_data(data_dir, 'c')
        is_ham = FLAGS.ham
        # learn_messages(fs, is_ham)

    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
