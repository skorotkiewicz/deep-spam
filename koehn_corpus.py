from __future__ import division

import os
import random
import numpy as np

import globals


def generate_features():
    print("Tokenizing/classifying email...")
    tokenize_files("corpus/ham", True)
    tokenize_files("corpus/spam", False)

    results = []
    results.extend(process_files("corpus/ham", True))
    results.extend(process_files("corpus/spam", False))

    print("Done. %d emails in corpus." % len(results))

    (train_data, test_data) = sample(filter(lambda r: len(r) == globals.FEATURE_COUNT + 1, results), 0.3)

    return (train_data[:, :globals.FEATURE_COUNT], train_data[:, globals.FEATURE_COUNT:globals.FEATURE_COUNT + 1]), \
           (test_data[:, :globals.FEATURE_COUNT], test_data[:, globals.FEATURE_COUNT:globals.FEATURE_COUNT + 1])


def tokenize_files(path, ham):
    filenames = list(map(lambda name: path + "/" + name, os.listdir(path)))
    for filename in filenames:
        tokens = globals.email_tokenizer.tokenize_email(open(filename))
        globals.token_classifier.classify_tokens(tokens, ham)


def process_files(path, ham):
    filenames = list(map(lambda name: path + "/" + name, os.listdir(path)))
    results = []
    i = 0
    ham_int = 1 if ham else 0
    for filename in filenames:
        tokens = globals.email_tokenizer.tokenize_email(open(filename))
        result = globals.email_feature_generator.generate_features_for_email(tokens, globals.token_classifier)
        result.append(ham_int)
        results.append(result)
        i += 1

    return np.array(results)


def sample(results, frac=0.3):
    train_data = []
    test_data = []
    for result in results:
        if random.random() > frac:
            train_data.append(result)
        else:
            test_data.append(result)

    return np.array(train_data), np.array(test_data)
