from __future__ import division

import os
import random
import numpy as np

from email_feature_generator import EmailFeatureGenerator, EmailTokenizer, TokenClassifier


def generate_features():
    email_feature_generator = EmailFeatureGenerator(64)
    email_tokenizer = EmailTokenizer()
    token_classifier = TokenClassifier()

    filename_to_spam = map_spam("CSDMC2010_SPAM/SPAMTrain.label")
    filenames = get_filenames("CSDMC2010_SPAM/TRAINING")

    for filename in filenames:
        tokens = email_tokenizer.tokenize_email(open("CSDMC2010_SPAM/TRAINING/" + filename))
        is_ham = filename_to_spam[filename]
        token_classifier.classify_tokens(tokens, is_ham)

    token_id_to_spamminess = token_classifier.finish_calculations()
    # token_id_to_token = {v: k for k, v in token_classifier.token_to_token_id.iteritems()}
    # for key, value in sorted(token_id_to_spamminess.iteritems(), key=lambda (k,v): (v,k)):
    #     print "%s: %s" % value, (token_id_to_token[key])

    results = []
    i = 0
    for filename in filenames:
        tokens = email_tokenizer.tokenize_email(open("CSDMC2010_SPAM/TRAINING/" + filename))
        is_ham = filename_to_spam[filename]
        result = email_feature_generator.generate_features_for_email(tokens, token_classifier)
        result.append(1 if is_ham else 0)
        results.append(result)
        i += 1

    results = np.array(results)

    (train_data, test_data) = sample(results, 0.3)

    return (train_data[:, :64], train_data[:, 64:65]), (test_data[:, :64], test_data[:, 64:65])


def map_spam(filename):
    f = open(filename)
    lines = f.readlines()
    spam_map = {}
    for line in lines:
        (flag, name) = line.split()
        spam_map[name] = (flag == "1")

    f.close()
    return spam_map


def get_filenames(path):
    return os.listdir(path)


def sample(results, frac=0.3):
    train_data = []
    test_data = []
    for result in results:
        if random.random() > frac:
            train_data.append(result)
        else:
            test_data.append(result)

    return np.array(train_data), np.array(test_data)
