#
# Generates features for an email


from __future__ import division
import datetime
import json
import redis
import globals


class EmailFeatureGenerator:
    def __init__(self, sample_size):
        self.sample_size = sample_size

    def generate_features_for_email(self, tokens, token_classifier):

        token_to_token_entry, spam_percentage = token_classifier.get_all_token_entries(tokens)
        token_to_spamminess = {}
        for token in token_to_token_entry:
            token_to_spamminess[token] = \
                token_classifier.calculate_spamminess(token_to_token_entry[token], spam_percentage)

        def compare(t1, t2):
            result = token_to_spamminess[t1] - token_to_spamminess[t2]
            if result < 0:
                return -1
            if result > 0:
                return 1
            return 0

        tokens_by_spamminess = sorted(list(tokens), cmp=compare)
        spammiest_tokens = tokens_by_spamminess[0:int(self.sample_size / 2)]

        len_t_b_s = len(tokens_by_spamminess) - 1
        hammiest_tokens = tokens_by_spamminess[len_t_b_s - len(spammiest_tokens): len_t_b_s]

        combined_tokens = list(spammiest_tokens)
        combined_tokens.extend(hammiest_tokens)

        combined_token_indices = list(map(lambda v: tokens.index(v), combined_tokens))

        token_id_to_index = {}
        for i in range(len(combined_tokens)):
            token_id_to_index[combined_tokens[i]] = combined_token_indices[i]

        combined_tokens.sort(key=lambda v: token_id_to_index[v])

        result = list(map(lambda v: token_to_spamminess[v], combined_tokens))
        # pad with 0.5 (neutral) at the end
        return result + [0.5] * (128 - len(result))

class EmailTokenizer:
    def tokenize_email(self, f):
        lines = f.readlines()
        tokens = []
        for line in lines:
            tokens.extend(line.split())

        return tokens


class TokenClassifier:
    def __init__(self, data_dir, mode):
        self.r = redis.Redis(host="localhost", port="6379", db=0)

    def classify_tokens(self, tokens, is_ham):
        spam_token_count = 0
        token_count = 0

        # map from token to number of instance of that token
        token_to_count = {}
        for token in tokens:
            count = token_to_count.get(token)
            if count is None:
                count = 1
            else:
                count += 1
            token_to_count[token] = count

        # fetch all the token_entries from redis at once
        pipeline = self.r.pipeline()
        for token in token_to_count:
            pipeline.get(token)
        token_jsons = pipeline.execute()
        token_entries = list(map(lambda e: None if e is None else json.loads(e), token_jsons))

        # update all the token entries in the file
        key = 'ham_count' if is_ham else 'spam_count'
        for (i, token) in enumerate(token_to_count):
            count = token_to_count[token]
            token_entry = token_entries[i]

            if token_entry is None:
                if not is_ham:
                    spam_token_count += 1
                token_count += 1
                token_entry = {'spam_count': 0, 'ham_count': 0}
                token_entries[i] = token_entry

            token_entry[key] += count
            token_entry['last_used'] = str(datetime.datetime.now())

        # send all the updates to redis at once
        pipeline = self.r.pipeline()
        for (i, token) in enumerate(token_to_count):
            pipeline.set(token, json.dumps(token_entries[i]))
        pipeline.incrby('magic: spam_token_count', spam_token_count)
        pipeline.incrby('magic: token_count', token_count)
        pipeline.execute()

    def get_all_token_entries(self, tokens):
        token_to_token_entry = {}
        for token in tokens:
            token_to_token_entry[token] = None
        pipeline = self.r.pipeline()
        for token in token_to_token_entry:
            pipeline.get(token)
        token_entries = list(map(lambda t: json.loads(t), pipeline.execute()))
        for (i, token) in enumerate(token_to_token_entry):
            token_to_token_entry[token] = token_entries[i]

        return token_to_token_entry, int(self.r.get('magic: spam_token_count')) / int(self.r.get('magic: token_count'))

    def calculate_spamminess(self, token_entry, spam_percentage):
        if token_entry is None:
            return 0.5

        spam_count = token_entry['spam_count']

        ham_count = token_entry['ham_count']

        spam_prob = spam_count / (spam_count + ham_count)
        return (spam_prob * spam_percentage) / (spam_prob * spam_percentage + (1 - spam_prob) * (1 - spam_percentage))
