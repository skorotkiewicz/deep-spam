# Deep Spam

Deep Spam is a deep learning network for classifying email into spam and non-spam categories.
It uses a combination of naive Bayesian prediction fed into a convolutional neural network to 
recognize common characteristics of spam (their "shape") rather than relying on a simple 
Bayesian formula to perform predictions. This increases the accuracy of the prediction engine
well above 99.99%, even using a 50/50 threshold to determine the class of the message. 

## Bayesian Model
In the Bayesian prediction portion of Deep Spam, trained messages are tokenized on whitespace
and mapped. Then, the messages are run through the system again, to determine the Bayesian
probability of each token being spam. The system takes the *n* tokens (n = 32) predicted most- and 
least-likely to be spam, and puts them into a feature vector *in the order they appear in the
message.* Preserving the word order allows the CNN to determine patterns in the way spam
messages are constructed, which turns out to be critical in prediction. 

## Deep Neural Network
The deep neural network is then trained on the messages by forming the feature vector into 
an image (64 x 128), convolving that into 32 feature maps, down-sampling those into 64 new feature
maps, downsampling again, and creating a two-layer fully-connected neural network, which results
in the two class outputs. This model is trained with the training data the order of 500 times.

In testing the model produces near-perfect accuracy (> 99.99%) on sample spam data obtained on 
the internet. Further testing is required to confirm the accuracy of the model.  

# The Software
Deep Spam is written in Python, with the machine learning bits implemented in TensorFlow. 
It keeps track of which tokens are 'spammy' and which aren't by keeping them in a Redis database. 

There are two basic components to Deep Spam:
1. A command line interface used to train the system and perform individual predictions; and
2. A Milter for integration with MTA/MDA like Postfix and Sendmail

# Using Deep Spam
1. Download the source files from [the repository](https://gitlab.koehn.com/bkoehn/deep-spam).
2. [Install Tensorflow](https://www.tensorflow.org/install/)
3. Install Redis or run the Docker image
4. Install (using `easy_install` or `pip`: `redis`, `numpy`
5. Run `python deep_spam.py -h` to learn the command line arguments
6. Train the system with a good-sized spam and ham corpus
7. Configure and run the Milter application for use with Postfix, Sendmail, etc.

Out of the box, TensorFlow doesn't take advantage of special instructions in your CPU for 
performing its calculations much more rapidly. If you 
[build it from source](https://www.tensorflow.org/install/install_sources) you can get 
performance several times faster. Note that this is only necessary when performing training;
the performance for predicting whether a message is spam or not is quite fast to begin with.

Deep Spam is **extremely** accurate; well over 99.99% once trained with an adequate 
amount of email and spam. It's very unlikely you will ever have a mis-classified message, but
if you do, you can always use the command line interface to update Deep Spam and it will learn
from its mistakes.  
