token_classifier = None
email_tokenizer = None
email_feature_generator = None

FEATURE_WIDTH = 8
FEATURE_HEIGHT = 8
FEATURE_COUNT = FEATURE_WIDTH * FEATURE_HEIGHT
